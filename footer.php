<!-- FOOTER START -->
<footer id="footer">
    <div class="footer">
        <div class="wrapper">
            <div class="footer1">
                <div class="titre">Dr Sabine Carton
                    <span>BE0829.147.288</span>
                    <span><a class="openPlan" title="Plan du site">Plan du site</a> - <a href="#" title="Mentions Légales">Mentions Légales</a></span>
                </div>
            </div>
            <div class="footer2">
                <ul>
                    <li class="adresse">Rue Verte Vallée, 14<span>–</span><br> 7387 Angre
                    <li><a href="tel:0470137164" title="0470 / 13 71 64">0470 / 13 71 64</a></li>
                    <li><a href="mailto:sabine.carton@hotmail.com" title="sabine.carton@hotmail.com">sabine.carton@hotmail.com</a></li>
                    
                </ul>
            </div>
            <div class="footer3">
                <div class="social">
                    <a title="Rejoignez-nous sur Facebook" target="_blank" class="facebook">Facebook</a>
                    <a href="header" title="Remonter" class="scroll scrollTop">Remonter</a>
                </div>
            </div>
            <div class="clear"></div>
        </div>
		<a href="http://www.toponweb.be/" title="Réalisé par Toponweb" target="_blank" class="toponweb">
        	<img src="images/toponweb.svg" alt="Top On Web" />
        </a>
    </div>
    <div class="plan">
        <a href="#" title="Page">Page</a>
        <a href="#" title="Page">Page</a>
        <a href="#" title="Page">Page</a>
        <a href="#" title="Page">Page</a>
        <a href="#" title="Page">Page</a>
    </div>
</footer>    
<!-- FOOTER END -->