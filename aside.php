<aside class="aside">
    <div class="blockService">
        <div>
            <span><img src="images/s1.svg" alt="Stérilisation" /></span>
            <h4 class="titre">Stérilisation</h4>
        </div>
        <div>
            <span><img src="images/s2.svg" alt="Détartrage" /></span>
            <h4 class="titre">Détartrage</h4>
        </div>
        <div>
            <span><img src="images/s3.svg" alt="Petite chirurgie" /></span>
            <h4 class="titre">Petite chirurgie</h4>
        </div>
        <div>
            <span><img src="images/s4.svg" alt="Examen microscopique" /></span>
            <h4 class="titre">Examen microscopique</h4>
        </div>
    </div>
    <div class="blockAnnulation">
    	<img src="images/img-aside.jpg" alt="Vétérinaire Carton">
        <a href="#" class="alim" title="Aliments & soins">
            AlimenTs & soins<i></i>
        </a>
        <a href="#" class="assur" title="Solutions d’assurances">
            Solutions d’assurances<i></i>
        </a>
    </div>
</aside>