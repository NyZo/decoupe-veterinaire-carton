<?php $namePage="pageAccueil"; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Vétérinaire Carton</title>
<meta name="description" content="Vétérinaire Carton" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->
<?php include "css/css.php";?>
</head>

<body>
<div id="wrapper">
    <?php include "header.php";?>
    <main id="homepage">

        <div class="blockService">
            <div class="wrapper">
                <div>
                    <span><img src="images/s1.svg" alt="Stérilisation" /></span>
                    <h2 class="titre">Stérilisation</h2>
                    <p>Class aptent taciti sociosqu ad litora ornare torquent per conubia nostra auctor inceptos himenaeos elit mauris in erat lusto nullam consequat.</p>
                </div>
                <div>
                    <span><img src="images/s2.svg" alt="Détartrage" /></span>
                    <h3 class="titre">Détartrage</h3>
                    <p>Class aptent taciti sociosqu ad litora ornare torquent per conubia nostra auctor inceptos himenaeos elit mauris in erat lusto nullam consequat.</p>
                </div>
                <div>
                    <span><img src="images/s3.svg" alt="Petite chirurgie" /></span>
                    <h3 class="titre">Petite chirurgie</h3>
                    <p>Class aptent taciti sociosqu ad litora ornare torquent per conubia nostra auctor inceptos himenaeos elit mauris in erat lusto nullam consequat.</p>
                </div>
                <div>
                    <span><img src="images/s4.svg" alt="Examen microscopique" /></span>
                    <h3 class="titre">Examen microscopique</h3>
                    <p>Class aptent taciti sociosqu ad litora ornare torquent per conubia nostra auctor inceptos himenaeos elit mauris in erat lusto nullam consequat.</p>
                </div>
                <a href="#" class="link" title="tous nos services ">tous nos services </a>
            </div>
        </div>

        <div class="blockAnnulation">
            <a href="#" class="tarifs" title="AlimenTs & soins">
                <span>AlimenTs & soins<i></i></span>
            </a>
            <div class="annuler">
                <p><span>Vétérinaire partenaire</span><br/> de la commune pour stériliser les chats errants. </p>
                <p>Récolte des bouchons en plastique pour les <span>amis des aveugles</span> de Ghlin<br/>
        <strong>(permet de financer les chiens d'aveugle)</strong></p>
            </div>
            <a href="#" class="faq" title="Solutions d’assurances">
                <span>Solutions d’assurances<i></i></span>
            </a>
            <div class="clear"></div>
        </div>

        <div class="bienvenueBloc wrapper">
            <h2 class="titre">Dr Sabine Carton<span>Ma plus grande satisfaction : aider votre animal à vivre mieux<br/> plus longtemps pour que vous puissiez parcourir un long chemin ensemble.</span></h2>
            
            <p>Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit nam nec tellus a odio tincidunt auctor a ornare odio. Sed non mauris vitae erat consequat auctor eu in elit. Class aptent taciti sociosqu ad litora torquent per lorem ipsum conubia nostra ac urna eu felis dapibus condimentum sit amet a augue. Conubia nostra auctor inceptos himenaeos elit mauris in erat lusto nullam consequat.</p>
            <a href="#" title="En savoir plus" class="link">En savoir plus</a>    
        </div>

        <div class="blockMap clr">
            <div class="map"></div>
            <div class="contentMap">
                <div>
                    <div class="titre">Consultation libre au cabinet</div>
                    <ul class="hour hour1">
                        <li>11h</li>
                        <li>12h</li>
                        <li>16h</li>
                        <li>18h</li>
                    </ul>
                    <ul class="graph graph1">
                        <li><em>Lun - Mer - Ven</em><span></span></li>
                    </ul>
                    <ul class="hour hour2">
                        <li>9h</li>
                        <li>12h</li>
                        <li>SUR RDV</li>
                    </ul>
                    <ul class="graph graph2">
                        <li><em>Mar</em><span></span></li>
                    </ul>
                    <ul class="hour hour3">
                        <li>9h</li>
                        <li>12h</li>
                        <li>16h</li>
                        <li>18h</li>
                    </ul>
                    <ul class="graph graph3">
                        <li><em>Jeu</em><span></span></li>
                    </ul>
                    <ul class="hour hour4">
                        <li>10h</li>
                        <li>12h</li>
                    </ul>
                    <ul class="graph graph4">
                        <li><em>Sam</em><span></span></li>
                    </ul>
                    <span>Possibilité de visites à domicile dans la région</span>
                </div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="blockPayement">
            <div class="wrapper">
                <p>Possibilité de livraison à domicile pour les aliments.</p>
            </div>
        </div>
    </main>
    <?php include "footer.php";?>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>