<style>
/* IMPORT FONTS */
@font-face {
  font-family: din-m;
  src: url('fonts/din-pro-medium.eot?#iefix') format('embedded-opentype'),  
       url('fonts/din-pro-medium.otf')  format('opentype'),
       url('fonts/din-pro-medium.woff') format('woff'), 
       url('fonts/din-pro-medium.ttf')  format('truetype'), 
       url('fonts/din-pro-medium.svg#din-pro-medium') format('svg');
  font-weight: normal;
  font-style: normal;
}

@font-face {
  font-family: gin-r;
  src: url('fonts/gineso-nor-eg.eot?#iefix') format('embedded-opentype'),  
       url('fonts/gineso-nor-eg.otf')  format('opentype'),
	     url('fonts/gineso-nor-eg.woff') format('woff'), 
       url('fonts/gineso-nor-eg.ttf')  format('truetype'), 
       url('fonts/gineso-nor-eg.svg#gineso-nor-eg') format('svg');
  font-weight: normal;
  font-style: normal;
}    
@font-face {
  font-family: din;
  src: url('fonts/din-pro-regular.eot?#iefix') format('embedded-opentype'),  
       url('fonts/din-pro-regular.otf')  format('opentype'),
	     url('fonts/din-pro-regular.woff') format('woff'), 
       url('fonts/din-pro-regular.ttf')  format('truetype'), 
       url('fonts/din-pro-regular.svg#din-pro-regular') format('svg');
  font-weight: normal;
  font-style: normal;
}

/* RESET */
body, ul, li, ol, form, h1, h2, h3, h4, h5, h6, div, span, p { padding:0; margin:0; border:0; -webkit-text-size-adjust:none; -moz-text-size-adjust:none; text-size-adjust:none;}
article, aside, dialog ,figcaption, figure, footer, header, hgroup, main, nav, section { display:block;}  
input, textarea	{ -webkit-appearance:none; -ms-appearance:none; appearance:none; -moz-appearance:none; -o-appearance:none; border-radius:0;}
*								{ outline:none !important;}
strong, b			  { font-weight:normal; font-family:din;}
ul					    { list-style-type:none;}
body						{ font: 16px/28px din; color:#222;}
a								{ text-decoration:none; color:#f89800; outline:none;}
img							{ border:none;}
#wrapper				{ min-width:320px; overflow:hidden; background:#fff; position: relative;}
#wrapper:before	{ content:''; display:block; z-index:99; position:absolute; top:0; right:0; bottom:0; left:0; background:rgba(0,0,0,0.80); opacity:0; visibility:hidden; transition:0.4s all ease-in-out !important; -webkit-transition:0.4s all ease-in-out !important;}
#wrapper.active:before { visibility:visible; opacity:1;}
#wrapper *			{ box-sizing:border-box;}

/* CLASS */
.clear					{ clear:both; display:block;}
.titre					{ font: 20px/32px gin-r; letter-spacing: 0.4px; text-transform: uppercase;  margin-bottom: 15px;}
.titre:after		{ content: ''; display: block; background: #4bb1c0; height: 2px; width:50px; margin: 15px auto 0;  }
.sousTitre			{ margin-bottom: 20px; font: 20px/32px gin-r; letter-spacing: .4px; color: #4bb1c0; text-transform: uppercase;}
.link						{ display:inline-block; width:auto; height:65px; position:relative; font: 16px/36px gin-r; letter-spacing: 0.4px; color:#222; text-decoration:none !important; padding:14px 32px 0; border: 1px #ccc solid; text-transform: uppercase; text-align: center;}
.wrapper				{ max-width: 1080px;padding:0 40px;  width: 100%; position: relative;margin: 0 auto;}
.clr:after			{ content: ''; display: table; width: 100%; clear: both;}

/* HEADER TOP */
#header							  { width:100%; position:relative; padding-top: 168px;}
#header.page          { padding-top: 170px;}
.headerTop						{ position: absolute; top:0; z-index: 100; width: 100%; background: #fff;}
.headerTop .right 		{ text-align: right; background: #f2f2f2; font: 16px/56px gin-r; letter-spacing: .4px;-webkit-transition:all 9 ease-in-out; -moz-transition:all 90ms ease-in-out; -ms-transition:all 90ms ease-in-out; transition:all 90ms ease-in-out;}
.slogan 	            { font:16px/32px gin-r; letter-spacing: .4px; margin-left: 60px; padding-top: 10px; float: left;}
.menuTop .slogan      { display:none;}
.headerTop .right .tel{ color:#fff; display:inline-block; vertical-align:middle; margin-right:-4px; background:url(images/icone-tel.svg) 29px 48% no-repeat #4bb1c0; height:56px; width:202px; text-align:left; padding-left:55px; float: right;}
.headerTop .social 		{ font-size:0; width:56px; height:56px; text-indent:-9999px; display:inline-block; cursor:pointer; float: right;}
.headerTop .facebook	{ background:url(images/icone-facebook.svg) 50% 50% no-repeat #fff; border: 1px #4bb1c0 solid; background-size: 7px;}
.headerTop .right .langue { text-align: center; width: 70px; background:url(images/drop.svg)  49px 50% no-repeat #222; display: inline-block; vertical-align: middle; color: #fff; padding-left: 23px; text-align: left;}
.btn-group 					  { display: none; position: absolute; width: 70px; right: 0; background: #222; z-index: 102; text-align: center; }
.btn-group a 				  { color: #fff; border-top: 1px solid #444; display: block;  }
.headerTop .logo			{ display: block; margin:15px 0 15px 60px ; float: left;}
.headerTop .logo img	{ width: 180px; display: block; }
.menuTop						  { background: #fff;}
.headerNav						{ float:right; position:relative; background:#fff; z-index:100;  margin:40px 60px 0 0 ; }
.menu 							  { width:100%; display:block; position:relative; text-align:center; background:#fff; font-size:0; line-height:0;}	
.menu li						  { display:inline-block; position:relative; margin:0 0 0 38px;}
.menu a							  { width:auto;  font: 16px/32px gin-r;letter-spacing: .4px; text-transform: uppercase; color:#222; display:block; padding:0;}
.menu>li.active>a			{ color:#4bb1c0;}
.sub							    { width:320px; position:absolute; top:32px; left:50%; z-index:995; margin-left:-160px; display:block;  visibility:hidden; opacity:0; padding-top: 40px}
.sub li							  { width:100%; margin:0; border:none; padding:0; text-align:center;}
.sub li a						  { width:100%; height:60px; line-height:60px; position:relative; border-bottom:1px solid #4bb1c0; margin:0;background:#edf7f9;}
.sub li:last-child a	{ border:0;}
.menu li:hover .sub		{ opacity:1; visibility:visible; z-index:999;}
.menu .sub li.active a{ background:#4bb1c0; color: #fff}

@media (min-width:1201px){
  .menuMobile						         { display:none;}
  .menu .vueMobile				       { display:none;}
  .menu							             { display:block !important;}
  .menu li:hover>a				       { color:#4bb1c0;}
  .menu .sub a:hover				     { background:#4bb1c0; color: #fff}
  .btn-group a:hover				     { color: #f89800 }
  .headerTop .logo:hover			   { opacity: .5 }
  .headerTop .facebook:hover     { background-color: #edf7f9;}
  .text-banner .actu:hover       { background: none !important; color: #4bb1c0;}
  .text-banner .link:hover,
  .headerTop .tel:hover          { background-color: #268f9e; border-color: #268f9e; color: #fff;}
}
@media (max-width:1200px){
  .slogan                        { margin-left: 40px;}
  .headerTop .logo               { margin: 15px 0 15px 40px; }
  .headerNav                     { width: 100%; margin: 0 }
  .menuMobile						         { width:auto; height:20px; display:block; z-index:80; transition:all 400ms ease-in-out; float:right; padding:0 40px 0 0; margin-top: 47px; cursor: pointer;}
  .menuMobile>div					       { width:auto; height:20px; position:relative; padding:0 28px 0 0;}
  .menuMobile span				       { width:28px; height:2px; background:#222; position:absolute; right:0; top:50%; display:block;}
  .menuMobile span:before			   { width:100%; height:2px; background:#222; position:absolute; right:0; top:8px; content:"";}
  .menuMobile span:after		     { width:100%; height:2px; background:#222; position:absolute; right:0; top:-8px; content:"";}
  .menuMobile.active span			   { height:0px;}
  .menuMobile.active span:before { top:0; transform:rotate(45deg);}
  .menuMobile.active span:after	 { top:0; transform:rotate(-45deg);}
  .menu							             { width:100%; height:auto; background:#edf7f9; border-top:1px solid 1px solid #48b3c1; display:none; position:absolute; left:0; top:0px;}
  .menu li						           { width:100%; display:block; margin:0;}
  .menu li a						         { height:64px; line-height:64px; padding:0 40px; margin:0; text-align:left; border-bottom:1px solid #333; color: #607D8B}
  .menu li a span					       { display:none;}
  .menu>li.active>a 			       { color:#4bb1c0;}
  .menu i 						           { display:block; width:100%; height:64px; position:absolute; right:0; top:0;}
  .menu i:before 					       { display:block; width:2px; height:20px; position:absolute; right:53px; top:50%; margin-top:-10px; content:""; background:#4bb1c0;transition:all 400ms ease-in-out;}
  .menu i:after 					       { display:block; width:22px; height:2px; position:absolute; right:43px; top:50%; margin-top:-1px; content:""; background:#4bb1c0;transition:all 400ms ease-in-out;}
  .menu i.active:before			     { display:none;}
  .menu li.active i:before	     { background:#fff;}
  .menu li.active i:after			   { background:#fff;}
  .menu .vueMobile 			         { display:block;}
  .menu .sub 						         { display:none; visibility:visible; width:100%; position:relative; left:auto; top:auto; margin:0; opacity:1; background:#f4f4f4; padding:0; border-bottom:1px solid #181818;}
  .menu .sub li a					       { background: #e8e8e8; text-align:left;}
  .menu .sub li.active a		     { color:#edf7f9;}
}
@media (max-width:820px){
  .headerTop .slogan             { display: none;}
  .hideMobile                    { display: none; }
}
@media (min-width:601px){
  .headerTop.fixedTop .menuTop   { position:fixed; width: 100%; top: 0; border-bottom: 1px #222 solid;}
}
@media (max-width:600px){
  #header                        { padding: 0;}
  .headerTop                     { position: relative;}
  .headerTop.fixedTop .menuTop   { border: none;}
  .menuMobile						         { padding:0 25px;}
  .menu li a						         { padding:0 25px; margin:0;}
  .menu i:before 					       { right:38px;}
  .menu i:after 					       { right:28px;}
  .headerTop .logo               { margin: 15px 0 15px 20px;}
  #header.page                   { padding-top: 0px;}
}

<?php if ($namePage == "pageAccueil") { ?>
/* HOMEPAGE */
.banner							    { width:100%; height:500px; min-height: 500px; position:relative; z-index:40; background:url(images/banner.jpg) 50% 50% no-repeat; -webkit-background-size:cover; -moz-background-size:cover; background-size:cover; text-transform: uppercase;}
.text-banner					  { display: block; max-width: 578px; min-height: 205px; background: rgba(0,0,0,.75); color: #fff; margin: auto; padding: 28px 42px 0; position: relative; top: 31%;}
.text-banner .actu 		  { display: block; padding: 4px 0; position: relative; font: 30px/36px gin-r; letter-spacing: .4px; color: #fff;}
.text-banner .actu:before { content: ''; background: url(images/icon-fotdog.svg) 50% no-repeat; width: 15px; height: 15px; display: inline-block; vertical-align: middle; margin:0 15px 7px 0;}    
.text-banner .link 		  { display: block; background:#4bb1c0; max-width: 365px; width: 100%; height:60px; margin: 0 auto; padding:13px 32px 0; border: none; text-align: center; position: absolute; bottom: 0; left: 0; right: 0; color:#fff;}    
.text-banner .link:after{ content: ''; display: inline-block; background: url(images/arw-next.svg) center no-repeat; width: 15px; height: 11px; margin-left: 14px;}

.blockService .wrapper  { padding: 90px 0; text-align: center;}
.blockService .wrapper div { padding: 0 80px 50px; display: inline-block; vertical-align: top; width: 50%; margin-left: -4px; text-align: center; }
.blockService span      { display: block; background: #edf7f9; width: 80px; height: 80px; margin: 0 auto 22px; border: 1px #bfc9cb solid; position: relative;}    
.blockService img       { display: block; position: absolute; top: 0; right: 0; bottom: 0; left: 0; margin: auto;}
.blockService a 			  { color: #4bb1c0;}
.blockService .wrapper .link { color: #222 !important;}

.blockAnnulation                { font: 18px/28px gin-r; letter-spacing: 0.4px; text-transform: uppercase; }
.blockAnnulation .annuler       { float:left; width:33.33%; background: url(images/deco.svg) center no-repeat #303030; height:380px; color:#fff; padding:70px 40px 0 60px;}
.blockAnnulation .annuler p     { background: url(images/icon-liste.svg) 0 8px no-repeat; padding-left: 50px; letter-spacing: 0 !important;}
.annuler p:first-of-type        { margin-bottom: 65px;}
.blockAnnulation .annuler span  { color: #4bb1c0;}
.blockAnnulation a              { float: left; width: 33.33%; height: 380px; position: relative; font: 20px/32px gin-r; text-align: center; background: #4bb1c0;}
.blockAnnulation a:before       { content: ''; width: 100%; height: 300px; display: block; }
.blockAnnulation a.tarifs:before{ background: url(images/bg-alim-soin.jpg) center no-repeat; background-size: cover;}
.blockAnnulation a.faq:before   { background: url(images/bg-assur.jpg) center no-repeat; background-size: cover;}
.blockAnnulation a span         { display: block; width: 100%; height: 80px; position: absolute; bottom: 0; left: 0; color: #fff; padding-top: 24px;}
.blockAnnulation .annuler strong{ font-size: 16px; text-transform: lowercase !important;}
.blockAnnulation i              { display: inline-block; background: url(images/arw-next.svg) center no-repeat; width: 15px; height: 11px; margin-left: 14px;}

.bienvenueBloc                  { width:100%; padding:80px 40px; text-align:center; position:relative; margin:0 auto;}
.bienvenueBloc .titre           { margin: 0 auto; margin-bottom: 25px; padding-bottom: 50px; font: 30px/24px gin-r; color: #4bb1c0; position: relative;}
.bienvenueBloc .titre:before    { content: ''; background: url(images/bg-titre.png) 50% no-repeat #fff; width: 93px; height: 23px; margin: 0 auto; padding:0 12px; position: absolute; left: 0; right: 0; bottom: 2px; z-index: 5;}
.bienvenueBloc .titre:after     { height: 1px; width: 200px; position: absolute; left: 0; right: 0; bottom: 12px;}
.bienvenueBloc .titre span      { display: block; margin-top: 25px; font-size: 16px; line-height: 24px; color: #222;}
.bienvenueBloc p                { padding:0 0 22px 0;}
.bienvenueBloc img              { margin: 0 auto 22px; display: block;}
.bienvenueBloc .link 				    { margin-top: 20px;}

.blockMap                       { background: #fcfcfc; height: 382px;}
.map                            { background: url(images/bg-map.jpg) no-repeat center #e8e8e8; max-width: calc( 100% - 455px); width: 77%; height: 382px; float: left;}
.contentMap                     { max-width: 455px; width:455px; height: 382px; float: right; position: relative;}
.contentMap>div                 { padding: 40px 0 30px;}
.contentMap .titre              { margin-bottom: 20px; font: 16px/28px gin-r; color: #4bb1c0; text-align: center;}
.contentMap .titre:before       { content: ''; background: url(images/icon-clock.svg) 50% no-repeat; width: 25px; height: 25px; display: inline-block; vertical-align: middle; margin: 0 20px 3px 0;}
.contentMap .titre:after        { display: none;}
.contentMap .hour,
.contentMap .graph              { padding: 0 60px 0 0; line-height: 0; text-align: right;}
.contentMap .hour li            { display: inline-block; padding-left: 10px; font: 12px/24px din;}
.contentMap .hour li:nth-child(2) { padding-left: 9px;}
.contentMap .hour li:nth-child(3) { padding-left: 22px;}
.contentMap .hour li:last-child { padding-left: 28px;}
.contentMap .graph              { margin-bottom: 6px;}
.contentMap .graph li           { padding: 0; font-family: din;}
.contentMap .graph em           { margin-right: 20px; font-style: normal; vertical-align: baseline;}
.contentMap .graph span         { height: 9px; display: inline-block; vertical-align: middle; width: 186px; border: 1px #48b3c1 solid; position: relative;}
.contentMap .graph span:before  { content: ''; position: absolute; width: 30px; height: 100%; background: #48b3c1; top: 0; left: 38.46%;}
.contentMap .graph span:after   { content: ''; position: absolute; width: 55px; height: 100%; background: #48b3c1; top: 0; right: 0;}
.graph1 span:before             { content: ''; position: absolute; width: 30px; height: 100%; background: #48b3c1; top: 0; left: 51px !important;}
.hour2 li:last-child            { padding: 0 32px 0 9px !important;}
.hour2 li:nth-child(2), 
.hour3 li:nth-child(2)          { padding-left: 62px !important;}
.graph2 span:before,
.graph3 span:before             { width: 81px !important; left: 0 !important;}
.graph2 span:after,
.graph4 span:after              { display: none !important;}
.hour4 li:last-child            { padding:0 97px 0 26px !important;}
.graph4 span:before             { width: 56px !important; left: 25px !important;}
.contentMap>div>span            { display: block; background: #4bb1c0; width: 100%; height: 97px; padding-top: 35px; position: absolute; left:0; bottom: 0; text-align: center; font: 18px/24px gin-r; color: #fff; text-transform: uppercase;} 

.blockPayement                  { letter-spacing: .4px; font: 20px/24px gin-r; text-align: center; text-transform: uppercase;color: #222;}
.blockPayement .wrapper         { height: 121px; display: table; }
.blockPayement p                { display: table-cell; vertical-align: middle; }
.blockPayement p:before         { content: ''; background: url(images/icon-car.svg) 50% no-repeat; width: 50px; height: 27px; display: inline-block; vertical-align: top; margin-right: 30px;}

@media (min-width:1201px){
  .text-banner .actu:hover            { background-color: #268f9e;}
  .blockService a:hover h2            { color: #f89800 }
  .blockService a:hover               { color: inherit; }
  .blockAnnulation a:hover            { background: #268f9e;}
}
@media (max-width:1320px){  
  .blockAnnulation .annuler           { padding: 70px 20px 0 30px;}
}
@media (max-width:1200px){  
  .blockService .wrapper div          { padding: 0 40px 50px;}
}
@media (max-width:1160px){  
  .blockAnnulation .annuler           { padding: 45px 20px 0 20px;}
}
@media (max-width:987px){  
  .blockAnnulation a                  { width: 50%;}
  .blockAnnulation .annuler           { width: 50%; padding: 45px 20px 0 20px;}
  .faq                                { display: none;}
}
@media (max-width:780px){
  .blockService .wrapper div          { display: block; width: 100%; text-align: center; }
  .annuler p:first-of-type            { margin-bottom: 45px;}
  .blockMap                           { height: inherit;}
  .map, .contentMap                   { max-width: 100%; width:100%; float: none;}
  .contentMap                         { height: 360px;}
  .contentMap>div                     { padding: 30px 0; max-width: 330px; margin: 0 auto; width: 100%;}
  .contentMap .hour, .contentMap .graph { padding: 0;}
  .contentMap>div>span                { padding: 35px 20px 0;}
}
@media (max-width:600px){
  .banner                             { background: url(images/banner-mobile.jpg) top no-repeat; background-size: cover; height: 240px; min-height: auto}
  .text-banner                        { border-right: 0; border-left: 0; top: 95px; max-width: 100%;}
  .text-banner .actu                  { font: 20px/24px gin-r;}
  .text-banner .link                  { max-width: 100%; text-align: left;}
  .text-banner .link:after            { height: 90px; position: absolute; right: 20px; top: 0; bottom: 0; margin: auto;}
  .blockAnnulation a                  { float: none; width: 100%; display:block;}
  .blockAnnulation .annuler           { background: #303030; float: none; width: 100%; height: inherit; padding: 35px 20px;}
  .blockAnnulation a.faq              { display: none; }
  .blockAnnulation a span             { text-align: left; padding-left:20px; position: relative;}
  .blockAnnulation i                  { position: absolute; top: 35px; right: 20px }
  .bienvenueBloc                      { display: none;}
  .blockPayement                      { height: auto; padding-top: 10px; padding-bottom: 10px;}
  .blockService .titre:after          { display: none;}
  .blockMap                           { height: auto; background: transparent; padding: 0; border: none; }
  .map                                { display: none;}
  .blockService .titre:after          { display: none; }
}
@media (max-width:400px) {
  .text-banner                        { padding: 16px 42px 0;}
  .blockService .wrapper              { padding-bottom: 35px;}
  .blockService .wrapper div          { display: table; width: 100%; padding: 0; position: relative; height: inherit; margin-bottom: 20px; text-align: left; padding: 0 20px;}
  .blockService .titre                { vertical-align: middle; display: inline-block; vertical-align: middle; text-align: left; font: 18px/32px gin-r;}
  .blockService span                  { display: inline-block; vertical-align: middle;  margin: 5px auto 5px; margin-right: 10px;}
  .blockService p                     { display: none;}
  .contentMap>div                     { max-width: 307px;}
  .contentMap .graph em               { margin-right: 10px;}
}
<?php } ?>

<?php if ($namePage != "pageAccueil") { ?>
/* PAGES INTERIEURES */
.banner-page              { background: url(images/banner-page.jpg) center no-repeat; background-size: cover; height: 230px }
.bredCrumb                { margin-bottom: 35px; padding-left: 0;}
.bredCrumb a              { color: #222; line-height: 60px; letter-spacing: .75px;}
.bredCrumb .active        { color:#4bb1c0;}
.bredCrumb .active:after{ display: none;}
.bredCrumb a:after        { content: '>'; display: inline-block; vertical-align: middle; margin: 0 10px; margin-top: -3px; font-family: gin-r;}
.titrePage				        { display:block; margin-bottom: 10px; font: 30px/24px gin-r; letter-spacing: .4px; text-transform: uppercase; color: #4bb1c0;}
.blocTop .sousTitre       { font: 18px/22px gin-r; color: #222;}
.chapo                    { margin-bottom: 45px; font: normal 18px/28px din-m;}
.pageContent				      { width:100%; position:relative; padding:0 300px 0 0;  min-height:1085px;}
.blocTop .sousTitre:after { content: ''; display: block; background: #4bb1c0; height: 2px; width:50px; margin: 27px 0 0; }
.blocPhoto                { max-width: 718px; width: 100%; margin: 0 auto; margin-bottom: 40px; line-height: 0; font-size: 0;}
.blocPhoto img            { display: inline-block; vertical-align: top; width: 49.9%;}
.blocPhoto img:last-of-type { margin-left: 1px;}
.pageLeft                 { width:100%; max-width:780px; padding: 30px 40px 40px; margin: auto;}
.pageLeft p				        { margin:0 0 42px 0;}
.pageLeft ul				      { margin:0 0 15px 0; padding:0 20px 0 50px;}
.pageLeft ul li			      { line-height:23px; padding:8px 0 8px 28px; background:url(images/icon-fotdog.svg) 0 13px no-repeat;}
.blocText2 p              { margin: 0 0 18px 0;}

.aside						        { width:320px; position:absolute; right:0; top:0;font: 20px/28px ald; letter-spacing: 0.5px;  text-transform: uppercase; z-index: 1;}
.blockService             { padding-top: 5px;}
.blockService>div         { padding: 15px 0; border-bottom: 1px #ccc solid;}
.blockService>div:last-of-type { border: none;}
.blockService span        { display: inline-block; vertical-align: middle; background: #edf7f9; width: 60px; height: 60px; margin-right: 25px; border: 1px #bfc9cb solid; position: relative;}    
.blockService img         { display: block; position: absolute; top: 0; right: 0; bottom: 0; left: 0; margin: auto;}
.blockService .titre      { display: inline-block; vertical-align: middle; margin-bottom: 0; text-transform: capitalize;}
.blockService .titre:after{ display: none;}
.aside .alim,.aside .assur{ background: #4bb1c0; font: 20px/32px gin-r;border-bottom: 1px solid #fff; display: block; color: #fff; position: relative; height: 90px; padding: 27px 0 0 20px;}
.aside .alim i,
.aside .assur i           { display: inline-block; background: url(images/arw-next.svg) center no-repeat; width: 15px; height: 11px; position: absolute; top: 38px; right: 20px;}
.aside img                { display: block;}

@media (max-width:1023px){
  .aside, .pageContent:after            { display: none;}
  .pageContent                          { padding: 0; min-height: auto;}
  .pageLeft                             { max-width: 100%;}
}
@media (max-width:600px){
  .banner-page                          { display: none;}
  .pageContent                          { border-top: 1px #ccc solid;}
  .pageLeft                             { padding: 35px 20px;}
  .blocPhoto img                        { width: 49%;}
  .pageLeft ul                          { padding: 0 20px 0 0;}
  .aside .reservez                      { padding: 22px 0 0 20px; height: 90px; border: none;}
  .aside .reservez i                    { height: 90px; right: 20px;}
}
@media (max-width:400px) {
  .blocPhoto img                        { width: 100%;}
  .blocPhoto img:last-of-type           { display: none;}
}
<?php } ?>

/* FOOTER */
#footer					          { width:100%; height:auto; position:relative; ; background:#222222;}
.footer						        { width:100%; position:relative; color:#fff;letter-spacing: .5px;}
.footer:before            { content: ''; background: #303030; width: 35%; position: absolute; top: 0; bottom: 0; left: 0;}
.footer .wrapper          { max-width: 1200px; padding-top: 80px; padding-bottom: 115px;}
.footer a					        { color:#fff;}
.footer1 					        { float:left; margin-right: 195px;}
.footer1 .titre				    { font-size: 28px; line-height: 24px; color: #fff}
.footer1 .titre:after     { display: none; }
.footer1 .titre>span		  { display: block; color:#fff; font: 14px/24px gin-r; letter-spacing: .4px; margin-top: 10px;}
.footer1 .titre>span:last-of-type { margin-top: 3px !important;}
.footer1 .titre .openPlan { cursor:pointer;}
.footer1 .titre a         { text-decoration: underline;}
.footer2  					      { float:left; padding:0 40px; }
.footer2 li 	 		        { padding:0 0 8px 30px; background: url(images/icon-fotdog.svg) left 5px no-repeat; font: 16px/24px gin-r;}
.footer2 .adresse br      { display: none; }
.footer3 					        { position:absolute; right:40px; top:82px;}
.social 					        { font-size:0;}
.social a 				        { width:56px; height:56px; text-indent:-9999px; display:inline-block; margin-left:10px; cursor:pointer; border: 1px #fff solid;}
.social .facebook			    { background:url(images/icone-facebook.svg) 50% 50% no-repeat; background-size: 7px}
.social .scrollTop		    { background:url(images/arw-top.svg) 50% 48% no-repeat;}
.toponweb				          { width:120px; height:60px; display:block; z-index:85; position: fixed; bottom: 0; right: 0; background: #4bb1c0; padding: 20px 0 0; opacity: 0; -webkit-transition:all 400ms ease-in-out; -moz-transition:all 400ms ease-in-out; -ms-transition:all 400ms ease-in-out; transition:all 400ms ease-in-out; }
.toponweb img			        { width:76px; display:block; margin:0 auto;}
.fixed 						        { opacity: 1;}
.plan						          { width:100%; position:relative; padding:16px 120px; background:#fff; text-align:center; overflow:hidden; display:none;}
.plan a					          { padding:0 5px; color:#222; font-size:17px;}

@media (max-width:1366px){
  .footer:before                        { width: 32%;}
}
@media (max-width:987px) {
  .footer:before                        { width: 37%;}
  .footer2 .adresse br                  { display: block;}
  .footer2 .adresse span                { display: none;}
  .footer1                              { margin-right: 100px; padding-top: 15px;}
}
@media (max-width: 899px) {
  .footer1                              { margin-right: 40px;}
}
@media (max-width:760px) {
  .footer:before,
  .footer1 .titre > span span           { display: none;}
  .footer1, .footer2                    { float: none;padding: 0; margin-right: 0;}
  .footer3                              { right: 20px; top: 49px }
  .footer .wrapper                      { padding-top: 49px;padding-bottom: 72px; }
}
@media (max-width:600px) {
  .wrapper                              { padding: 0 20px } 
  .social .google                       { display: none; }
  .plan                                 { padding: 16px 0 80px;}
}
@media (max-width: 400px) {
  .footer3                              { right: 0; top: 10px; position: relative; left: 0;}
  .social a                             { margin-left: 0; margin-right: 8px;}
}

/* HOVER EFFECTS */
@media (min-width:1201px){
  body a, body a span, span:before, span:after, a:after, a:before, .link, .sub, .slick-prev, .slick-next, .slick-dots button, .serviceBloc *, h2 { -webkit-transition:all 400ms ease-in-out; -moz-transition:all 400ms ease-in-out; -ms-transition:all 400ms ease-in-out; transition:all 400ms ease-in-out;}
  a:hover								   { color:#4bb1c0;}
  .scrollBot a:hover			 { opacity:.5;}
  .social a:hover					 { background-color:#268f9e;}
  .link:hover, .tel:hover	 { background-color:#edf7f9; border-color:#edf7f9; color:#4bb1c0;}
  .serviceBloc:hover .hide { opacity:1;}
  .hide .link:hover				 { background-color:#fff; color:#4bb1c0;}
  .slick-prev:hover, .slick-next:hover{ opacity:.5;}
  .footer a:hover					 { color:##49b2c1;}
  .toponweb:hover 				 { background:#268f9e;}
  .contentMap a:hover      { color: #fff; background: #49b2c1;}
  .aside .alim:hover       { background: #268f9e;}
  .aside .assur:hover      { background: #268f9e;}
  .pageLeft a:hover			   { color:#4bb1c0;}
  .bienvenueBloc p a:hover { color:#d56a04}
  .blockService a:hover  	 { color:#d56a04}
} 
</style>