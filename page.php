<?php $namePage="pagePresentation"; $nameSub="pageNone"; ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Vétérinaire Carton</title>
<meta name="description" content="Vétérinaire Carton" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
<!--[if lt IE 9]>
<script src="js/html5.js"></script>
<![endif]-->

<?php include "css/css.php";?>
</head>

<body>
<div id="wrapper">
<?php include "header.php";?>

<main class="pageContent">
	<div class="pageLeft">
        <div class="blocTop">
            <div class="bredCrumb"><a href="" title="Accueil">Accueil</a><a title="Présentation" class="active">Présentation</a></div>
    		<div class="titrePage">Dr Sabine Carton</div>
            <h1 class="sousTitre">Vétérinaire petits animaux</h1>
            <h2 class="chapo">Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor nam at nisi elit lorem consequat ipsum, nec sagittis sem nibh elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris morbi accumsan ipsum.</h2>
        </div>
        <div class="blocText blocText1">
            <h2 class="sousTitre">sed do eiusmod tempor incididunt</h2>
            <p>Equitis Romani autem esse filium criminis loco poni ab accusatoribus neque his iudicantibus oportuit neque defendentibus nobis. Nam quod de pietate dixistis, est quidem ista nostra existimatio, sed iudicium certe parentis; quid nos opinemur, audietis ex iuratis; quid parentes sentiant, lacrimae matris incredibilisque maeror, squalor patris et haec praesens maestitia, quam cernitis, declarat.</p>
            <h2 class="sousTitre">ut labore et dolore magna aliqua</h2>
            <p>Lorem ipsum dolor sit amet adipicing elit  gravida nibh velit auctor aliquet sollicitudin, lorem quis bibendum auctor nisi consequat ipsum, nec sagittis sem nibh id elit odio elit sit amet.</p>
        </div>
        <div class="blocPhoto">
            <img src="images/photo-page1.jpg" alt="Vétérinaire">
            <img src="images/photo-page2.jpg" alt="Vétérinaire">
        </div>
        <div class="blocText blocText2">
            <h2 class="sousTitre">ut labore et dolore magna aliqua</h2>
            <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor nam at nisi elit lorem consequat ipsum, nec sagittis sem nibh elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris morbi accumsan ipsum velit nam nec tellus tincidunt auctor.</p>
            <p>Ultima Syriarum est Palaestina per intervalla protenta :</p>
            <ul>
                <li>Nam nibh vel velit auctor aliquet ipsum nec sagittis sem nibh,</li>
                <li>Aenean sollicitudin, lorem quis bibendum auctor nam at nisi elit,</li>
                <li>Lorem consequat ipsum morbi accumsan ipsum tellus tincidunt auctor,</li>
                <li>Proin gravida nibh vel velit auctor aliquet lorem quis bibendum auctor nam.</li>
            </ul>
            <p>Nam nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor nam at nisi elit lorem consequat ipsum nec sagittis sem nibh morbi accumsan ipsum velit nam nec tellus tincidunt auctor. Morbi accumsan ipsum velit nam nec tellus velit auctor aliquet.</p>
        </div>
	</div>
	<?php include "aside.php";?>

</main>

<?php include "footer.php";?>
</div>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
</body>
</html>