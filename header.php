<!-- HEADER START -->
<header id="header" <?php if ($namePage != "pageAccueil") echo("class='page'"); ?>>
    <div class="headerTop clr">
        <div class="right clr">
            <?php if ($namePage == "pageAccueil") { ?><h1 class="slogan">Vétérinaire pour petits animaux à Angre</h1><?php } ?>
            <?php if ($namePage != "pageAccueil") { ?><div class="slogan">Vétérinaire pour petits animaux à Angre</div><?php } ?>
            <a href="tel:0470137164" title="0470 / 13 71 64" class="tel">0470 / 13 71 64</a>
            <a href="#" class="social facebook" title="Suiver-nous sur facebook"></a>
        </div>
        <div class="menuTop">
            <div class="menuMobile"><div><span></span></div></div>
            <a href="homepage.php" class="logo" title="Vétérinaire Carton"><img src="images/logo.svg" alt="Vétérinaire Carton" /></a>           
            <nav class="headerNav">
                <ul class="menu">
                    <li<?php if ($namePage == "pageAccueil") echo(" class='active'"); ?>><a href="homepage.php" title="Accueil">Accueil<span></span></a></li>
                    <li<?php if ($namePage == "pagePresentation") echo(" class='active'"); ?>><a href="page.php" title="Présentation">Présentation<span></span></a></li>
                    <li<?php if ($namePage == "pageServices") echo(" class='active'"); ?>><a href="page.php" title="Services">Services<span></span></a><i></i>
                        <ul class="sub">
                            <li class="vueMobile"><a href="#" title="Vue d'ensemble">Vue d'ensemble</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Page">Page</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?> class="active"><a href="page.php" title="Page">Page</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Page">Page</a></li>
                            <li <?php if ($namePage == "pageCon") echo (" class='active'"); ?>><a href="page.php" title="Page">Page</a></li>
                        </ul>
                    </li>
                    <li<?php if ($namePage == "pageVente") echo(" class='active'"); ?>><a href="page.php" title="Vente produits">Vente produits<span></span></a></li>
                    <li<?php if ($namePage == "pageAssurances") echo(" class='active'"); ?>><a href="page.php" title="Assurances">Assurances<span></span></a></li>
                    <li<?php if ($namePage == "pageActualités") echo(" class='active'"); ?>><a href="page.php" title="Actualités">Actualités<span></span></a></li>
                    <li<?php if ($namePage == "pageFAQ") echo(" class='active'"); ?>><a href="page.php" title="FAQ">FAQ<span></span></a></li>
                    <li<?php if ($namePage == "pageContact") echo(" class='active'"); ?>><a href="page.php" title="Contact">Contact<span></span></a></li>
                </ul>
            </nav>
            <div class="clear"></div>
        </div>
    </div>
	<?php if ($namePage == "pageAccueil") { ?>
    <div class="banner">
        <div class="text-banner">
            <a href="#" class="actu" title="Consultations libres ou sur RDV">Consultations libres ou sur RDV</a>
            <a href="#" class="actu" title="Visites à domicile & urgences">Visites à domicile & urgences</a>
            <a href="#" title="Prenez rendez-vous dès maintenant" class="link">Prenez rendez-vous dès maintenant</a>
        </div>
        
    </div>
    <?php } else { ?>
        <div class="banner-page"></div>
    <?php } ?>
</header>
<!-- HEADER END -->