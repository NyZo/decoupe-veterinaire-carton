$(document).ready(function() {	
	// MENU MOBILE //
	$(".menuMobile").click(function() {
		$(this).toggleClass("active");
		$(".menu").fadeToggle();
		$(".sub").css('display','none');
		$(".menu li i").removeClass('active');
		$('#wrapper').toggleClass('active');
	});	
	$(".menu li i").click(function() {
		$(this).toggleClass('active');
		$(".menu").find('.sub' ).slideUp();
		if($(this).hasClass("active")){
			$(".menu li i").removeClass('active')
			$(this).next().slideToggle();
			$(this).toggleClass('active');
		}
	});

	// SCROLL //
	$(".scroll").click(function() {
		c = $(this).attr("href")
		$('html, body').animate({ scrollTop: $("#" + c).offset().top + 1}, 1000, "linear");
		return false
	})

	// MENU FIXED //
	 $(window).scroll(function() {
	  var scroll = $(window).scrollTop();  
		 if (scroll > 56) { 
		  $('.headerTop').addClass('fixedTop');
		 }   
		 else {
		  $('.headerTop').removeClass('fixedTop');
		 }
	 });

	// TOW LOGO //
	 $(window).scroll(function() {
	  var scroll = $(window).scrollTop(); 
	  var header = $('#header').height() - 600;  
		 if (scroll < header) { 
		  $('.toponweb').removeClass('fixed');
		 }   
		 else {
		  $('.toponweb').addClass('fixed');
		 }
	 });

	// PLAN DU SITE //
	$(".openPlan").click(function() {
		$(".plan").slideToggle()
		$("html, body").animate({scrollTop: $(document).height()}, "slow");
	});	
});